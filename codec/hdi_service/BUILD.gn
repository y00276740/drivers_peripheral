# Copyright (c) 2021 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import("//build/ohos.gni")
import("//drivers/hdf_core/adapter/uhdf2/uhdf.gni")

ohos_shared_library("libcodec_client") {
  include_dirs = [
    "//drivers/hdf_core/adapter/uhdf2/include/hdi",
    "//drivers/peripheral/codec/hdi_service/common",
    "//drivers/peripheral/codec/interfaces/include",
  ]
  sources = [
    "codec_proxy/codec_callback_proxy.c",
    "codec_proxy/codec_proxy.c",
    "codec_proxy/proxy_msgproc.c",
    "common/common_msgproc.c",
  ]

  if (is_standard_system) {
    external_deps = [
      "c_utils:utils",
      "drivers_peripheral_display:hdi_gralloc_client",
      "graphic_chipsetsdk:buffer_handle",
      "hdf_core:libhdf_ipc_adapter",
      "hdf_core:libhdf_utils",
      "hdf_core:libhdi",
      "hiviewdfx_hilog_native:libhilog",
    ]
  } else {
    external_deps = [ "hilog:libhilog" ]
  }

  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_codec"
}

group("codec_client") {
  deps = [ ":libcodec_client" ]
}

ohos_shared_library("libcodec_server") {
  include_dirs = [
    "//drivers/peripheral/codec/interfaces/include",
    "//drivers/peripheral/codec/hdi_service/codec_proxy",
    "//drivers/peripheral/codec/hdi_service/common/",
    "//drivers/peripheral/codec/hal/v1.0/codec_instance/include",
    "//drivers/peripheral/codec/hal/v1.0/share_mem/include",
    "//drivers/peripheral/codec/hal/v1.0/oem_interface/include",
    "//drivers/peripheral/codec/hal/v1.0/buffer_manager/include",
  ]
  sources = [
    "//drivers/peripheral/codec/hal/v1.0/codec_instance/src/codec_instance.c",
    "//drivers/peripheral/codec/hal/v1.0/share_mem/src/ashmem_wrapper.cpp",
    "//drivers/peripheral/codec/hal/v1.0/share_mem/src/share_mem.c",
    "codec_service_stub/codec_callback_service.c",
    "codec_service_stub/codec_callback_stub.c",
    "codec_service_stub/codec_config_parser.c",
    "codec_service_stub/codec_host.c",
    "codec_service_stub/codec_service.c",
    "codec_service_stub/codec_stub.c",
    "codec_service_stub/stub_msgproc.c",
    "common/common_msgproc.c",
  ]

  deps = [ "//drivers/peripheral/codec/hdi_service:codec_client" ]

  if (is_standard_system) {
    external_deps = [
      "c_utils:utils",
      "drivers_peripheral_display:hdi_gralloc_client",
      "graphic_chipsetsdk:buffer_handle",
      "hdf_core:libhdf_host",
      "hdf_core:libhdf_ipc_adapter",
      "hdf_core:libhdf_utils",
      "hiviewdfx_hilog_native:libhilog",
    ]
  } else {
    external_deps = [ "hilog:libhilog" ]
  }

  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_codec"
}

group("codec_service") {
  CODEC_OEM_PATH = rebase_path(
          "//device/soc/${device_company}/${product_name}/hardware/codec")
  cmd = "if [ -f ${CODEC_OEM_PATH}/BUILD.gn ]; then echo true; else echo false; fi"
  HAVE_CODEC_OEM_PATH =
      exec_script("//build/lite/run_shell_cmd.py", [ cmd ], "value")

  if (HAVE_CODEC_OEM_PATH) {
    deps = [ ":libcodec_server" ]
  }
}
